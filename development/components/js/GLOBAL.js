//============================
//    Name: index.js
//============================

// import for others scripts to use
window.$ = $;
window.jQuery = jQuery;

// import 'jquery-mask-plugin';

import 'inputmask/dist/min/jquery.inputmask.bundle.min';


// import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';

$(() => {

	/* ======= Global Actions ======= */
	const
		k = 'click',
		act = 'item-active';
	/* ======= ---- ======= */


 
 
	/* ======= list ======= */
	const ___order__list_ = () => {
		pageMain.__load();
	};
	/* ======= ---- ======= */

	const pageMain = {
		__load() {
			// let regexpEmail = /\S+[@]\S+[.]\w+$/gsi;

			let setting = {

				// * Run functions once
				__firstRuns() {
					this.analitica();
					this.switchPhoto();
					this.phoneMask();
				},
				// * Handlers
				__handlers() {
					$('.js__navMenu').on(k, () => this.navMenu());
					$('.js__formSender').on(k, () => this.sendForm());
					$('.js__phoneMask').on(k, () => this.phoneMaskClick());
					$('.js__phoneMask').on('focusout', () => this.phoneMaskLost());
					$('.js__phoneMask').on('change keypress', () => this.phoneMaskChange());
				},

				/* ======= Code ======= */
				analitica() {
					let gbot = `
					<!-- GOOGLEBOT -->
					<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119535751-1"></script>
					<script>
					window.dataLayer = window.dataLayer || [];
					function gtag(){ dataLayer.push(arguments); }
					gtag("js", new Date());
					gtag("config", "UA-119535751-1");
					</script>
					`;
					$('head').append(gbot);
				},
				
				sendForm() {

					let nextStep = () => {
						let form_data = $('#form-phone-number').serialize();
						$.ajax({
								url: 'email-sender.php',
								type: 'POST',
								dataType: 'html',
								data: form_data,
							})
							.done(() => console.log('send') + this.formSendBtnEvent())
							.fail(() => console.log('fail'))
							.always(() => console.log('end'));
					};
     
 
					$('.js__phoneMask').val().indexOf('-') >= 0 || $('.js__phoneMask').val().length <= 0
						? setTimeout(() => $('.js__phoneHint').attr('data-tooltips', 'false'), 2000, $('.js__phoneHint').attr('data-tooltips', 'true'))
						: nextStep();
						
				},

				formSendBtnEvent() {
					let addItem = (x, v) => $('.js__formContainer').children().eq(x).addClass(`anima-phone-${v}`);
					let removeItem = (x) => $('.js__formContainer').children().eq(x);

					setTimeout(() => removeItem(0).fadeOut(), 100, addItem(0, 'true'));
					setTimeout(() => removeItem(1).removeClass('page-phone__done'), 500, addItem(1, 'false'));
				},

				phoneMaskChange() {
					// let zz = $('.js__phoneMask').val().indexOf('-');
					// console.log('inputSize: ', zz);
				},

				phoneMaskLost() {
					// $('.js__phoneMask').val(' ');
				},
				
				phoneMaskClick() {
					// $('.js__phoneMask').val('+375 (');

					$.fn.setCursorPosition = function(pos) {
						this.each(function(index, elem) {
							if (elem.setSelectionRange) {
								elem.setSelectionRange(pos, pos);
							} else if (elem.createTextRange) {
								var range = elem.createTextRange();
								range.collapse(true);
								range.moveEnd('character', pos);
								range.moveStart('character', pos);
								range.select();
							}
						});
						return this;
					};
					$('.js__phoneMask').focus().setCursorPosition(19);

				},

				phoneMask() {
					$('.js__phoneMask').inputmask({
						'mask': '+375 (99) 999 99 99',
						'placeholder': '-'
					});
				},

				navMenu() {
					$('.js__navMenuToggle, .js__navMenu').toggleClass(act);
				},

				switchPhoto() {

					let
						takePhoto = '.js__itemPhoto',
						origin = '.js__generatorPhoto',
						clone = '.js__generatorPhotoCloned';


					let setBackground = () => {

						let item = () => $(takePhoto).children(`.${act}`).data('bg-url');
						let itemNext = () =>
							$(takePhoto).children(`.${act}`).next().length === 0
							? $(takePhoto).children().first().data('bg-url')
							: $(takePhoto).children(`.${act}`).next().data('bg-url');


						$(origin).attr('style', `background-image: url(${item()})`);
						$(clone).attr('style', `background-image: url(${itemNext()})`);


						// let zz = () => $(origin).addClass('fadeMyImage');
						// let xx = () => $(origin).removeClass('fadeMyImage');

						setTimeout(() => $(origin).addClass('fadeMyImage'), 4500);

					};





					setBackground();
					setInterval(() => {
						let nextItem = $(`.page__bg-item.${act}`).removeClass(act).next('.page__bg-item');
						nextItem.length === 0 ? nextItem = $('.page__bg-item').first() : '';
						nextItem.addClass(act);
						setBackground();
						$(origin).removeClass('fadeMyImage');
					}, 5500);

				},

				scrollUPShow() {
					$(window).scrollTop() >= 100
						? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act)
						: $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
				},

				/* ======= ---- ======= */




				// * Loaders 
				__loaders() {
					this.__firstRuns();
					this.__handlers();
				}
			};
			setting.__loaders();
		}
	};
	/* ======= list ======= */
	___order__list_();
	/* ======= ---- ======= */
});