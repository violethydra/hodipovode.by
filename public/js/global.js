webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery, __webpack_provided_window_dot_jQuery) {

__webpack_require__(2);

//============================
//    Name: index.js
//============================
// import for others scripts to use
window.$ = $;
__webpack_provided_window_dot_jQuery = jQuery; // import 'jquery-mask-plugin';

// import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';
$(function () {
  /* ======= Global Actions ======= */
  var k = 'click',
      act = 'item-active';
  /* ======= ---- ======= */

  /* ======= list ======= */

  var ___order__list_ = function ___order__list_() {
    pageMain.__load();
  };
  /* ======= ---- ======= */


  var pageMain = {
    __load: function __load() {
      // let regexpEmail = /\S+[@]\S+[.]\w+$/gsi;
      var setting = {
        // * Run functions once
        __firstRuns: function __firstRuns() {
          this.analitica();
          this.switchPhoto();
          this.phoneMask();
        },
        // * Handlers
        __handlers: function __handlers() {
          var _this = this;

          $('.js__navMenu').on(k, function () {
            return _this.navMenu();
          });
          $('.js__formSender').on(k, function () {
            return _this.sendForm();
          });
          $('.js__phoneMask').on(k, function () {
            return _this.phoneMaskClick();
          });
          $('.js__phoneMask').on('focusout', function () {
            return _this.phoneMaskLost();
          });
          $('.js__phoneMask').on('change keypress', function () {
            return _this.phoneMaskChange();
          });
        },

        /* ======= Code ======= */
        analitica: function analitica() {
          var gbot = "\n\t\t\t\t\t<!-- GOOGLEBOT -->\n\t\t\t\t\t<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-119535751-1\"></script>\n\t\t\t\t\t<script>\n\t\t\t\t\twindow.dataLayer = window.dataLayer || [];\n\t\t\t\t\tfunction gtag(){ dataLayer.push(arguments); }\n\t\t\t\t\tgtag(\"js\", new Date());\n\t\t\t\t\tgtag(\"config\", \"UA-119535751-1\");\n\t\t\t\t\t</script>\n\t\t\t\t\t";
          $('head').append(gbot);
        },
        sendForm: function sendForm() {
          var _this2 = this;

          var nextStep = function nextStep() {
            var form_data = $('#form-phone-number').serialize();
            $.ajax({
              url: 'email-sender.php',
              type: 'POST',
              dataType: 'html',
              data: form_data
            }).done(function () {
              return console.log('send') + _this2.formSendBtnEvent();
            }).fail(function () {
              return console.log('fail');
            }).always(function () {
              return console.log('end');
            });
          };

          $('.js__phoneMask').val().indexOf('-') >= 0 || $('.js__phoneMask').val().length <= 0 ? setTimeout(function () {
            return $('.js__phoneHint').attr('data-tooltips', 'false');
          }, 2000, $('.js__phoneHint').attr('data-tooltips', 'true')) : nextStep();
        },
        formSendBtnEvent: function formSendBtnEvent() {
          var addItem = function addItem(x, v) {
            return $('.js__formContainer').children().eq(x).addClass("anima-phone-".concat(v));
          };

          var removeItem = function removeItem(x) {
            return $('.js__formContainer').children().eq(x);
          };

          setTimeout(function () {
            return removeItem(0).fadeOut();
          }, 100, addItem(0, 'true'));
          setTimeout(function () {
            return removeItem(1).removeClass('page-phone__done');
          }, 500, addItem(1, 'false'));
        },
        phoneMaskChange: function phoneMaskChange() {// let zz = $('.js__phoneMask').val().indexOf('-');
          // console.log('inputSize: ', zz);
        },
        phoneMaskLost: function phoneMaskLost() {// $('.js__phoneMask').val(' ');
        },
        phoneMaskClick: function phoneMaskClick() {
          // $('.js__phoneMask').val('+375 (');
          $.fn.setCursorPosition = function (pos) {
            this.each(function (index, elem) {
              if (elem.setSelectionRange) {
                elem.setSelectionRange(pos, pos);
              } else if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
              }
            });
            return this;
          };

          $('.js__phoneMask').focus().setCursorPosition(19);
        },
        phoneMask: function phoneMask() {
          $('.js__phoneMask').inputmask({
            'mask': '+375 (99) 999 99 99',
            'placeholder': '-'
          });
        },
        navMenu: function navMenu() {
          $('.js__navMenuToggle, .js__navMenu').toggleClass(act);
        },
        switchPhoto: function switchPhoto() {
          var takePhoto = '.js__itemPhoto',
              origin = '.js__generatorPhoto',
              clone = '.js__generatorPhotoCloned';

          var setBackground = function setBackground() {
            var item = function item() {
              return $(takePhoto).children(".".concat(act)).data('bg-url');
            };

            var itemNext = function itemNext() {
              return $(takePhoto).children(".".concat(act)).next().length === 0 ? $(takePhoto).children().first().data('bg-url') : $(takePhoto).children(".".concat(act)).next().data('bg-url');
            };

            $(origin).attr('style', "background-image: url(".concat(item(), ")"));
            $(clone).attr('style', "background-image: url(".concat(itemNext(), ")")); // let zz = () => $(origin).addClass('fadeMyImage');
            // let xx = () => $(origin).removeClass('fadeMyImage');

            setTimeout(function () {
              return $(origin).addClass('fadeMyImage');
            }, 4500);
          };

          setBackground();
          setInterval(function () {
            var nextItem = $(".page__bg-item.".concat(act)).removeClass(act).next('.page__bg-item');
            nextItem.length === 0 ? nextItem = $('.page__bg-item').first() : '';
            nextItem.addClass(act);
            setBackground();
            $(origin).removeClass('fadeMyImage');
          }, 5500);
        },
        scrollUPShow: function scrollUPShow() {
          $(window).scrollTop() >= 100 ? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act) : $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
        },

        /* ======= ---- ======= */
        // * Loaders 
        __loaders: function __loaders() {
          this.__firstRuns();

          this.__handlers();
        }
      };

      setting.__loaders();
    }
  };
  /* ======= list ======= */

  ___order__list_();
  /* ======= ---- ======= */

});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0), __webpack_require__(0), __webpack_require__(0)))

/***/ })
],[1]);